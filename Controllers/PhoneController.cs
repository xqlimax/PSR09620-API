using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PSR09620.API.Data;
using PSR09620.API.Models;

namespace PSR09620.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhoneController : ControllerBase
    {
        private readonly IPhoneRepository _repo;

        public PhoneController(IPhoneRepository repo)
        {
            _repo = repo;
        }

        [HttpPost("add")]
        public async Task<IActionResult> Add(Phone phone)
        {
            phone.Brand = phone.Brand.ToLower();
            phone.Model = phone.Model.ToLower();

            if(await _repo.PhoneExist(phone.Brand,phone.Model))
                return BadRequest("Phone already exists");

            var phoneToCreate = new Phone
            {
                Brand = phone.Brand,
                Model = phone.Model,
                Price = phone.Price,
                ReleaseDate = phone.ReleaseDate
            };
            
            await _repo.AddPhone(phoneToCreate);
            return StatusCode(201);
        }
    }
}