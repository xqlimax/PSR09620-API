using System;
using System.ComponentModel.DataAnnotations;

namespace PSR09620.API.Models
{
    public class User
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte[] PasswordHash { get; set; }
        [Required]
        public byte[] PasswordSalt { get; set; }
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string LastName { get; set; }
    }
}