using System;
using System.ComponentModel.DataAnnotations;

namespace PSR09620.API.Models
{
    public class Phone
    {
        [Key]
        public Guid Id { get; set; }
        [Required, StringLength(32, MinimumLength = 1, ErrorMessage = "You must specify a name brand between 1 and 32 characters")]
        public string Brand { get; set; }
        [Required, StringLength(32, MinimumLength = 1, ErrorMessage = "You must specify a name model between 1 and 32 characters")]
        public string Model { get; set; }
        [Required(ErrorMessage="A price is required")]
        public float Price { get; set; }
        [Required(ErrorMessage="A Date is required"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ReleaseDate { get; set; }
    }
}