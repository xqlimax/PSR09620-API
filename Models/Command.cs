using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PSR09620.API.Models
{
    public class Command
    {
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }
        [Key]
        public Guid Id { get; set; }
        [ForeignKey("Phone")]
        public Guid PhoneId { get; set; }
        public Phone Phone { get; set; }

        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public User User { get; set; }
        [Required, MinLength(3), MaxLength(50)]
        public string Description { get; set; }
    }
}