using Microsoft.AspNetCore.Http;

namespace PSR09620.API.Helpers
{
    public static class Extension
    {
        //this method add headers to the error response and allow CORS requests
        public static void AddApplicationError(this HttpResponse response, string message)
        {
            response.Headers.Add("Application-Error", message);
            response.Headers.Add("Access-Control-Expose-Headers", "Application-Error");
            response.Headers.Add("Access-Control-Allow-Origin", "*");
        }
    }
}