using System.ComponentModel.DataAnnotations;

namespace PSR09620.API.Dtos
{
    public class UserForRegisterDto
    {
        [Required, EmailAddress]
        public string Email { get; set; }
        
        [Required, StringLength(8, MinimumLength = 4, ErrorMessage = "You must specify password between 4 and 8 characters")]
        public string Password { get; set; }

        [Required, StringLength(64, MinimumLength = 1, ErrorMessage = "You must specify name of maximum 64 charaters")]
        public string Name { get; set; }

        [Required, StringLength(64, MinimumLength = 1, ErrorMessage = "You must specify lastname of maximum 64 charaters")]
        public string LastName { get; set; }
    }
}