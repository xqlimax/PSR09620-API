using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PSR09620.API.Models;

namespace PSR09620.API.Data
{
    public class PhoneRepository : IPhoneRepository
    {
        private readonly PSR09620Context _context;

        public PhoneRepository(PSR09620Context context)
        {
            _context = context;
        }

        public async Task<bool> PhoneExist(string brand, string model)
        {
            var phone = await _context.Phone.FirstOrDefaultAsync(x => x.Brand == brand && x.Model == model);
            if(phone == null)
                return false;
            return true;
        }
        public async Task<Phone> AddPhone(Phone phone)
        {
            await _context.Phone.AddAsync(phone);
            await _context.SaveChangesAsync();
            return phone;
        }


    }
}