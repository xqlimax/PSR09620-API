using Microsoft.EntityFrameworkCore;
using PSR09620.API.Models;

namespace PSR09620.API.Data
{
    public class PSR09620Context : DbContext
    {
        public PSR09620Context(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Command>()
            .HasAlternateKey(c => c.Description)
            .HasName("Unique_Description");

            modelBuilder.Entity<User>()
            .HasAlternateKey(c => c.Email)
            .HasName("Unique_Email");
        }

        public DbSet<Command> Command { get; set; }
        public DbSet<User> User {get; set;}
        public DbSet<Phone> Phone { get; set; }
    }
}