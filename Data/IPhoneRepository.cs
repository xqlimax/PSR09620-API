using System;
using System.Threading.Tasks;
using PSR09620.API.Models;

namespace PSR09620.API.Data
{
    public interface IPhoneRepository
    {
        Task<bool> PhoneExist(string brand, string model);
        Task<Phone> AddPhone(Phone phone);
    }
}