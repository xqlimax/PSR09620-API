using System.Threading.Tasks;
using PSR09620.API.Models;

namespace PSR09620.API.Data
{
    public interface IAuthRepository
    {
         Task<User> Register(User user, string password);
         Task<User> Login(string email, string password);
         Task<bool> UserExists(string email);
    }
}